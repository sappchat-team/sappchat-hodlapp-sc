// SPDX-License-Identifier: MIT
pragma solidity 0.6.12;

import "@pancakeswap/pancake-swap-lib/contracts/token/BEP20/BEP20.sol";
import '@pancakeswap/pancake-swap-lib/contracts/token/BEP20/IBEP20.sol';

contract SappchatVirtualToken is BEP20('Sappchat virtual token', 'SAPPCHATV') {
    function mint(address _to, uint256 _amount) public onlyOwner {
        _mint(_to, _amount);
    }

    function burn(address _from ,uint256 _amount) public onlyOwner {
        _burn(_from, _amount);
    }

    IBEP20 public sappchat;

    constructor(
        IBEP20 _sappchat
    ) public {
        sappchat = _sappchat;
    }

    // Safe sappchat transfer function, just in case if rounding error causes pool to not have enough SAPPCHATs.
    function safeSappchatTransfer(address _to, uint256 _amount) public onlyOwner {
        uint256 sappchatBal = sappchat.balanceOf(address(this));
        if (_amount > sappchatBal) {
            sappchat.transfer(_to, sappchatBal);
        } else {
            sappchat.transfer(_to, _amount);
        }
    }
}

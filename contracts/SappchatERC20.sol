// SPDX-License-Identifier: MIT
pragma solidity 0.6.12;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract SappchatERC20 is ERC20 {
    constructor() ERC20("SAPPCHAT","SAPPCHAT") public {
        _mint(msg.sender, 1000000000 ether);
    }
}

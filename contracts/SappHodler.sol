// SPDX-License-Identifier: MIT

pragma solidity 0.6.12;
pragma experimental ABIEncoderV2;
import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/SafeERC20.sol";
import "@openzeppelin/contracts/utils/ReentrancyGuard.sol";

contract SappHodler is Ownable, ReentrancyGuard {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;
    // Info of each pool.
    struct PoolInfo {
        uint256 amount;
        uint256 rewardDebt;
        uint256 apy; // APY 6% = 6.
        uint256 depositedAt;
        uint256 duration;
        address owner;
        bool claimed;
    }
    // Pool setting
    struct PoolSetting {
        uint256 duration; // Duration.
        uint256 apy; // APY.
    }
    /// @notice The SAPPCHAT ERC-20 contract.
    IERC20 public immutable SAPPCHAT;
    /// @notice The SAPPCHAT ERC-20 contract.
    address public TREASURY;
    /// @notice Info of each user that stakes tokens.
    PoolInfo[] public poolInfo;
    /// @notice pool config setting
    mapping(uint256 => PoolSetting) public poolSetting;
    uint256 MAX_APY = 100;
    uint256 private constant ACC_SAPPCHAT_PRECISION = 1e12; // 6 digit after comma
    uint256 public allClaimed = 0;
    event Deposit(address indexed user, uint256 amount);
    event Withdraw(
        address indexed user,
        uint256 indexed pid,
        uint256 amount,
        address indexed to
    );
    event WithdrawAll(
        address indexed user,
        uint256[] indexed pids,
        uint256 amount,
        address indexed to
    );
     event EmergencyWithdraw(
        address indexed user,
        uint256 amount
    );
    event LogPoolAddition(
        uint256 indexed pid,
        uint256 amount,
        uint256 rewardDept,
        uint256 indexed apy,
        uint256 depositedAt,
        uint256 duration,
        address indexed owner
    );
    event LogUpdatePoolApySetting(uint256 indexed pid, uint256 apy);
    event LogUpdatePoolDurationSetting(uint256 indexed pid, uint256 duration);
    event LogUpdateTreasury(address indexed from, address indexed to);

    /// @param _SAPPCHAT the SAPPCHAT Token
    /// @param _TREASURY the SAPPCHAT treasury for reward
    constructor(address _SAPPCHAT, address _TREASURY) public {
        SAPPCHAT = IERC20(_SAPPCHAT);
        TREASURY = _TREASURY;
        poolSetting[0] = PoolSetting({duration: 5 minutes, apy: 3});
        poolSetting[1] = PoolSetting({duration: 10 minutes, apy: 8});
        poolSetting[2] = PoolSetting({duration: 15 minutes, apy: 25});
    }

    /// @notice Returns the number of pools.
    function poolLength() external view returns (uint256) {
        return poolInfo.length;
    }

    /// @notice Update the given pool's setting APY. Can only be called by the owner.
    /// @param _setting The index of the pool. See `poolInfo`.
    /// @param _apy new APY of the pool
    function updatePoolApySetting(uint256 _setting, uint256 _apy)
        external
        onlyOwner
    {
        require(
            poolSetting[_setting].apy != _apy && _apy > 0,
            "SAPPCHAT::updatePoolApySetting bad apy"
        );
        poolSetting[_setting].apy = _apy;
        emit LogUpdatePoolApySetting(_setting, _apy);
    }

    /// @notice Update the given pool's setting Duraion. Can only be called by the owner.
    /// @param _setting The index of the pool. See `poolInfo`.
    /// @param _duration new APY of the pool
    function updatePoolDurationSetting(uint256 _setting, uint256 _duration)
        external
        onlyOwner
    {
        require(
            poolSetting[_setting].duration != _duration && _duration > 0,
            "SAPPCHAT::updatePoolDurationSetting bad duration"
        );
        poolSetting[_setting].duration = _duration;
        emit LogUpdatePoolDurationSetting(_setting, _duration);
    }

    /// @notice Update the TREASURY.
    /// @param _treasury address of new TREASURY.
    function updateTreasury(address _treasury) external onlyOwner {
        require(
            _treasury != address(0) && _treasury != TREASURY,
            "SAPPCHAT::updateTreasury bad address"
        );
        address old = TREASURY;
        TREASURY = _treasury;
        emit LogUpdateTreasury(old, TREASURY);
    }

    /// @notice View function to see pending SAPPCHATs on frontend.
    /// @param _pid The index of the pool. See `poolInfo`.
    /// @param _user address of user
    function pendingSAPPCHAT(uint256 _pid, address _user)
        external
        view
        returns (uint256)
    {
        PoolInfo memory pool = poolInfo[_pid];
        require(pool.owner == _user, "SAPPCHAT::pendingSAPPCHAT not owner");
        uint256 _pendingSAPPCHAT = 0;
        uint256 stakeTokenSupply = SAPPCHAT.balanceOf(address(this));
        if (stakeTokenSupply != 0) {
            uint256 pass = block.timestamp - pool.depositedAt;
            if (pass > pool.duration) {
                pass = pool.duration;
            }
            if (pass > 0) {
                uint256 multiplier = pass.mul(ACC_SAPPCHAT_PRECISION).div(
                    pool.duration
                );
                _pendingSAPPCHAT = multiplier.mul(
                    pool.rewardDebt.div(ACC_SAPPCHAT_PRECISION)
                );
            }
        }
        return _pendingSAPPCHAT;
    }

    /// @notice Add a new pool.
    /// @param _amount APY of the new pool
    /// @param _poolSetting PoolSetting of the new pool
    function addPool(
        address _for,
        uint256 _amount,
        uint256 _poolSetting
    ) internal {
        uint256 apy = poolSetting[_poolSetting].apy;
        uint256 duration = poolSetting[_poolSetting].duration;
        require(apy > 0 && apy <= MAX_APY, "SAPPCHAT::addPool bad apy");
        uint256 reward = _amount.mul(apy).mul(ACC_SAPPCHAT_PRECISION).div(100);
        poolInfo.push(
            PoolInfo({
                amount: _amount,
                rewardDebt: reward,
                apy: apy,
                depositedAt: block.timestamp,
                duration: duration,
                owner: _for,
                claimed: false
            })
        );
        uint256 pid = poolInfo.length.sub(1);
        emit LogPoolAddition(
            pid,
            _amount,
            reward,
            apy,
            block.timestamp,
            duration,
            _for
        );
    }

    /// @notice Deposit SAPPCHAT to DiamondHand.
    /// @param _amount The amount of sappchat
    /// @param _poolSetting The pool setting
    function deposit(uint256 _amount, uint256 _poolSetting)
        external
        nonReentrant
    {
        // Validation
        require(msg.sender != address(0), "SAPPCHAT::deposit:: bad address");
        require(_amount > 0, "SAPPCHAT::deposit:: bad _amount");
        require(_poolSetting >= 0, "SAPPCHAT::addPool bad _poolSetting");
        require(
            msg.sender != address(TREASURY),
            "SAPPCHAT::deposit:: treasury excluded"
        );
        // Interactions
        SAPPCHAT.safeTransferFrom(address(msg.sender), address(this), _amount);
        addPool(msg.sender, _amount, _poolSetting);
        emit Deposit(msg.sender, _amount);
    }

    /// @notice Withdraw Sappchat from DiamondHand.
    /// @param _for Receiver
    /// @param pid The index of the pool. See `poolInfo`.
    function withdraw(address _for, uint256 pid) external nonReentrant {
        PoolInfo storage pool = poolInfo[pid];
        require(!pool.claimed, "SAPPCHAT::withdraw:: already claimed");
        require(pool.owner == msg.sender, "SAPPCHAT::withdraw:: only owner");
        require(pool.depositedAt != 0, "SAPPCHAT::withdraw:: time invalid");
        require(
            (pool.depositedAt + pool.duration) < block.timestamp,
            "SAPPCHAT::withdraw:: time invalid"
        );
        // Effects
        uint256 reward = pool.rewardDebt.div(ACC_SAPPCHAT_PRECISION);
        uint256 amount = pool.amount;
        pool.rewardDebt = 0;
        pool.amount = 0;
        pool.claimed = true;
        allClaimed++;
        // Interactions
        SAPPCHAT.safeTransferFrom(address(TREASURY), msg.sender, reward);
        SAPPCHAT.safeTransfer(msg.sender, amount);
        emit Withdraw(msg.sender, pid, reward, _for);
    }

    /// @notice emergencyWithdraw sappchat that somehow got stuck
    // Require all pool need to be claimed
    function emergencyWithdraw() external onlyOwner {
        require(
            poolInfo.length == allClaimed,
            "emergencyWithdraw: unclaimed pools"
        );
        uint256 amount = SAPPCHAT.balanceOf(address(this));
        SAPPCHAT.safeTransfer(msg.sender, amount);
        emit EmergencyWithdraw(msg.sender,amount);
    }
}

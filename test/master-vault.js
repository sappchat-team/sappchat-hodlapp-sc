const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("master", function () {
  it("Should return the new master once it's changed", async function () {

    const [owner,invester,invester2] = await ethers.getSigners();
    //console.log("Treasury address: " +owner.address);
    //console.log("Invester address: " +invester.address);
    const SappchatG = await ethers.getContractFactory("SappchatERC20");
    const sappchat = await SappchatG.deploy();
    await sappchat.deployed();

    const SappchatVirtualToken = await ethers.getContractFactory("SappchatVirtualToken");
    const sappchatv = await SappchatVirtualToken.deploy(sappchat.address);
    await sappchatv.deployed();


    await sappchat.approve(invester.address,1000000);
    await sappchat.transfer(invester.address,1000000);

    await sappchat.approve(invester2.address,1000000);
    await sappchat.transfer(invester2.address,1000000);
    
    let investerBalance = await sappchat.balanceOf(invester.address);
    console.log("Invester SAPPCHAT balance: " +investerBalance.toString());

    const MasterVault = await ethers.getContractFactory("MasterVault");
    const master = await MasterVault.deploy(sappchat.address,sappchatv.address,owner.address);
    await master.deployed();
    await master.updateMasterSupplier(owner.address);
    await sappchatv.transferOwnership(master.address);
    await sappchat.approve(master.address,18000000000000);

    let deposit = 17000;
    let duration = 30 * 24 * 60 * 60;

    await sappchat.connect(invester).approve(master.address,deposit);

    await master.connect(invester).enterStaking(deposit);
    

    for (let index = 0; index < 28800 / 2; index++) {
      await network.provider.send("evm_mine");
    }

    await sappchat.connect(invester2).approve(master.address,100000);

    await master.connect(invester2).enterStaking(100000);


    //await master.connect(invester).leaveStaking(deposit);

    //investerBalance = await sappchat.balanceOf(invester.address);
    //console.log("Invester SAPPCHAT balance: " +investerBalance.toString());

    let pending = await master.pendingSappchat(0,invester.address);
    let pending2 = await master.pendingSappchat(0,invester2.address);
    console.log(`Pending ` + pending.toString());
    console.log(`Pending ` + pending2.toString());
    
  });
});

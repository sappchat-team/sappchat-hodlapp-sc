const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Faucet", function () {
  it("Should return the new DiamondHand once it's changed", async function () {

    const [owner,begger] = await ethers.getSigners();
    console.log("owner address: " +owner.address);
    console.log("begger address: " +begger.address);
    const SappchatG = await ethers.getContractFactory("SappchatERC20");
    const sappchat = await SappchatG.deploy();
    await sappchat.deployed();

    let investerBalance = await sappchat.balanceOf(begger.address);
    console.log("begger SAPPCHAT balance: " +investerBalance.toString());

    const Faucet = await ethers.getContractFactory("SappchatTreasury");
    const faucet = await Faucet.deploy(sappchat.address);
    await faucet.deployed();
    
    await sappchat.approve(faucet.address,10000);
    await sappchat.transfer(faucet.address,10000);

    let faucetBalance = await faucet.balanceOf();
    console.log("faucetBalance SAPPCHAT balance: " +faucetBalance.toString());

    await faucet.faucet(begger.address);

    let b = await sappchat.balanceOf(begger.address);
    console.log("begger SAPPCHAT balance: " +b.toString());

    expect(b.toNumber()).to.equal(1000);
  });
});

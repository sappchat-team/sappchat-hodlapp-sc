const hre = require("hardhat");

async function main() {
  

  const SappHodler = await hre.ethers.getContractFactory("SappHodler");
  const master = await SappHodler.deploy("0x097F8aE21e81D4F248A2E2D18543c6b3CC0D8E59","0xdfcd54Ac1b1bE19789782D94661581D38a35F41E");

  await master.deployed();

  console.log("SappHodler deployed to:", master.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });


const hre = require("hardhat");

async function main() {
  
  const SappchatTreasury = await hre.ethers.getContractFactory("SappchatTreasury");
  const treasury = await SappchatTreasury.deploy("0x208e4E53f9872bC3636790dBDAD2E7B983894C2a");

  await treasury.deployed();

  console.log("SappchatTreasury deployed to:", treasury.address);

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });


const hre = require("hardhat");

async function main() {
    const SappchatG = await ethers.getContractFactory("SappchatERC20");
    const sappchat = await SappchatG.deploy();
    await sappchat.deployed();
    console.log("SappchatERC20 deployed to:", sappchat.address);
    const DiamondHand = await hre.ethers.getContractFactory("DiamondHand");
    const master = await DiamondHand.deploy(sappchat.address, "0xeFfe75B1574Bdd2FE0Bc955b57e4f82A2BAD6bF9");

    await master.deployed();

    console.log("DiamondHand deployed to:", master.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });

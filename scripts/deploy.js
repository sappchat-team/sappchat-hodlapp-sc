
const hre = require("hardhat");
// const abi = require("../abi/sappchat.abi.json")
async function main() {
  

  // const accounts = await hre.ethers.getSigners()
  // const sappchat = await ethers.getContractFactory("SappchatERC20");
  //const Sappchat = await ethers.getContractFactory("SappchatERC20");
  const sappchat = new ethers.Contract('0x77fDdfB3c526B9dfD3CE15f0359F98D0dcce5b0f', abi, accounts[0]);
  const sappchat = await Sappchat.deploy();
  // await sappchat.deployed();
  console.log("SappchatERC20 deployed to:", sappchat.address);
  const DiamondHand = await hre.ethers.getContractFactory("DiamondHand");
  console.log(11111111);
  const master = await DiamondHand.deploy(sappchat.address,"0xAb7D9763ee384C428e752c6A113c520061baD963");

  await master.deployed();

  console.log("DiamondHand deployed to:", master.address);
  let treasury = await sappchat.balanceOf("0xAb7D9763ee384C428e752c6A113c520061baD963");
  console.log("treasury balance :", treasury.toString());
  await sappchat.approve(master.address,treasury);


  let approval = await sappchat.allowance("0xAb7D9763ee384C428e752c6A113c520061baD963",master.address);
  console.log("approval balance :", approval.toString());
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
